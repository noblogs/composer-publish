build-container
===

Repository for common Gitlab CI rules to replease PHP Composer pacakges.

# Usage

- Make sure that the Package Repository is enabled for the project 
- Set up a project's *.gitlab-ci.yml* file with this include rule:

```yaml
include: "https://git.autistici.org/noblogs/composer-publish/raw/master/common.yml"
```
- Add like this notice for composer in composer.json

```
{
    "name": "noblogs/noblogs-home",
    "description": "Wordpress theme: Noblogs Buddypress home page.",
    "type": "wordpress-theme",
    "license": "MIT",
    "authors": [
        {
            "name": "shammash",
            "email": "shammash@autistici.org"
        },
        {
            "name": "agata",
            "email": "agata@insiberia.net"
        }	
    ],
    "require": {
        "composer/installers": "^1.9"
    }
}
```
